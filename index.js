$(function() {

    //--------HOME--------
    $('#nav_icon img').on('mouseover', function() {
        $(this).css({
            "background-color": '#3C787E',
            "opacity": '0.6'
        });
        
    });
    
    $('#nav_icon img').on('mouseleave', function() {
        $(this).css({
            "background-color": '',
            "opacity": ''
        });
        
    });

    $('#nav_icon img').on('click', function() {
        $("nav").slideToggle(100, function() {
            $(this).removeClass("hidden");
            $(this).css("background-color", " ");
        })
    });
    

    //--------Nav-------------
    const nav = $("nav");
    const menuLi = nav.find(".menu li");

    nav.find(".headermenu img").on('click', function() {
        nav.slideToggle(100, function (){
            $(this).addClass("hidden");
        });
    });

    menuLi.on("mouseover", function () {
        $(this).css("opacity", "0.4");
    });

    menuLi.on("mouseleave", function () {
        $(this).css("opacity", "1");
    });

    menuLi.on("click", function (event) {
        const indexClicked = $(this).closest("li").index();
        const menuLink = $(this).data("menulink");
        const newPosition = $(menuLink).offset();
        const newtop = newPosition.top - 95;

        nav.slideToggle(100, function (){
            $(this).addClass("hidden");
            $("html, body").stop().animate({ scrollTop: newtop}, 600);
        });
        event.preventDefault();
    });



    


    //-------About-----------
    $("#skills .fields li").on("mouseover", function () {
        $(this).css("opacity", "0.4");
    });

    $("#skills .fields li").on("mouseleave", function () {
        $(this).css("opacity", "1");
    });

    $("#skills .fields li").on('click', function() {
        const field = $(this).data("field");
        const wrapperSelected = $(".field_wrapper.selected");
        const wrapperClicked = $(`.field_wrapper.${field}`);

        $("#skills .fields li").removeClass("selected");
        $(this).addClass("selected");
        
        if (!wrapperSelected.hasClass(field)) {
            wrapperClicked.removeClass("hidden");
            wrapperSelected.slideDown(100, function () {
                wrapperSelected.addClass("hidden");
                wrapperClicked.addClass("selected");
                wrapperSelected.removeClass("selected");
            });
        }
        
    });



    //-------Works-----------
    function setWorksBg(init) {
        const bg = $("#bg_works");
        if (init === true) {
            bg.css("background-color","#60495A");
        }
        const bgHeight = bg.height();
        const bgWidth = bg.width();
        const pxShorten = Math.tan(2*Math.PI/180)*bgWidth;
        const percentage = Math.ceil(100*pxShorten/bgHeight);
        bg.css({
            "clip-path" : `polygon(0 ${percentage}%, 100% 0, 100% 100%, 0 ${100-percentage}%)`,
            "padding" : `${pxShorten+20}px 0`
        });
    }

    setWorksBg(true);

    $(window).resize(function () {
        setWorksBg(false);
    });

    $(".categories").change(function () {
        const category = $(this).children("option:selected").val();

    });

    const $worksLayer = $("#works .img .layer");

    $worksLayer.on("mouseover", function () {
        $(this).css("opacity", "0.8");
    });

    $worksLayer.on("mouseleave", function () {
        $(this).css("opacity", "0.5");
    });

    $worksLayer.on("click", function () {
        const url = $(this).data("url");
        // window.open(url, "_blank");
        $("<a>").prop({
            target: "_blank",
            href: url
        })[0].click();

    });

    
    //-----------BLOGS----------
 
    const $blog = $(".blog");

    $blog.on("mouseover", function () {
        $(this).css({
            "box-shadow": "rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px"   
        });
    });

    $blog.on("mouseleave", function () {
        $(this).css({
            "box-shadow": "rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px"
        });
    });


    //---------Contact---------
    function setContactBg(init) {
        const bg = $("#bg_contact");
        if (init === true) {
            bg.css("background-color","#60495A");
        }
        const bgHeight = bg.height();
        const bgWidth = bg.width();
        const pxShorten = Math.tan(2*Math.PI/180)*bgWidth;
        const percentage = Math.ceil(100*pxShorten/bgHeight);
        bg.css({
            "clip-path" : `polygon(0 ${percentage}%, 100% 0, 100% 100%, 0 100%)`,
            // "padding" : `${pxShorten+20}px 0`
        });
        console.log(bgHeight);
        console.log(bgWidth);
    }

    setContactBg(true);

    $(window).resize(function () {
        setContactBg(false);
    });

    const $contactType = $("#contenttype");
    let onSelecting = false;
    
    $contactType.on("mouseover", function () {
        if (onSelecting === false) {
            const lenOption = $(this).find("option").length;   
            const $option = $("#contact option");
            $option.css({"position":"relative",
            "top":"40px"});
            $contactType.css({"height":"125px"});
            $contactType.attr("size", lenOption);  
        }
    });

    $contactType.on("click", function () {
        $contactType.css("height","35px");
        $contactType.attr("size", 1);
    });

    const $sendButton = $(".form_wrapper button");

    $sendButton.on("mouseover", function () {
        $(this).css({
            "background-image": "linear-gradient(30deg,#8a7384 5%, #60495A 95%)",
            "border": "2px solid rgb(255, 255, 255, 0.8)"
        });
        console.log("hey");
    });

    $sendButton.on("mouseleave", function () {
        $(this).css({
            "background-image": "linear-gradient(30deg,#8a7384 5%, #60495A 50%)",
            "border": "2px solid rgb(255, 255, 255, 0.6)"

        });
    });

    $sendButton.on("click", function () {
        $(this).css({
            "background-image": "none",
            "background-color": "#8a7384",
            "border": "2px solid rgb(255, 255, 255, 1)"
        });
    });


});
